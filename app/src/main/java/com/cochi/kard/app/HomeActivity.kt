package com.cochi.kard.app

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.HORIZONTAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cochi.kard.R
import com.cochi.kard.app.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: HomeViewModel

    private val actionBar: Toolbar get() = toolbar
    private val transactionList: RecyclerView get() = transaction_list
    private val swipeRefresh: SwipeRefreshLayout get() = refresh

    private val transactionAdapter = TransactionAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        appComponent.provideHomeComponent().create().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(actionBar)
        actionBar.title = getString(R.string.app_name)

        transactionList.run {
            layoutManager =
                LinearLayoutManager(this@HomeActivity, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    this@HomeActivity,
                    LinearLayoutManager.VERTICAL
                )
            )
            setHasFixedSize(true)
            setItemViewCacheSize(20)
            adapter = transactionAdapter.apply { setHasStableIds(true) }
        }

        swipeRefresh.setOnRefreshListener {
            viewModel.synchronise()
        }

        viewModel.getStateLiveData().observe(
            this, Observer { applyState(it as HomeViewModel.State) }
        )

        viewModel.getUsers()
    }

    private fun applyState(state: HomeViewModel.State) {
        when (state.step) {
            is HomeViewModel.Step.EmptyState -> { /*show empty state*/ }
            is HomeViewModel.Step.UserFetched -> {
                /* show users, fetch data for one */
                val users = state.step.users
                if (users.isNotEmpty()) viewModel.synchronise(users.random().id)
            }
            is HomeViewModel.Step.DisplayTransaction -> {
                swipeRefresh.isRefreshing = false
                transactionAdapter.items = state.step.transactions
            }
            HomeViewModel.Step.Loading -> swipeRefresh.isRefreshing = true

        }
    }
}