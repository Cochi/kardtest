package com.cochi.kard.app.base

import androidx.appcompat.app.AppCompatActivity
import com.cochi.kard.KardApp

abstract class BaseActivity : AppCompatActivity() {

    protected val appComponent get() = (application as KardApp).appComponent
}