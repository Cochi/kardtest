package com.cochi.kard.app

import com.cochi.kard.core.base.BaseFlowableInteractor
import com.cochi.kard.core.base.BaseSingleInteractor
import com.cochi.kard.core.realm.DatabaseHandler
import com.cochi.kard.data.Transaction
import com.cochi.kard.data.TransactionRepository
import com.cochi.kard.data.User
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetTransactionInteractor @Inject constructor(
    private val transactionRepository: TransactionRepository
) : BaseFlowableInteractor<List<Transaction>>(
    AndroidSchedulers.from(DatabaseHandler.looper), AndroidSchedulers.mainThread()
) {
    private var id: String = ""

    override fun getFlowable(): Flowable<List<Transaction>> {
        return transactionRepository.getTransactions(id)
    }

    fun getTransaction(
        userId: String,
        onSuccess: (List<Transaction>) -> Unit,
        onError: (Throwable) -> Unit
    ): Disposable {
        id = userId
        return subscribe(onSuccess, onError)
    }
}