package com.cochi.kard.app.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel<S> : ViewModel() {

    private val tag = this.javaClass.simpleName

    abstract val defaultState: S
    private val stateLiveData by lazy { MutableLiveData<S>() }

    private val compositeDisposable = CompositeDisposable()

    fun getStateLiveData() : LiveData<S> = stateLiveData

    protected fun reduce(reducer: (state: S) -> S) {
        stateLiveData.value = reducer.invoke(stateLiveData.value ?: defaultState)
    }

    protected fun toCompositeDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun error(throwable: Throwable){
        Log.e(tag, throwable.message, throwable)
    }

    override fun onCleared() {
        super.onCleared()
        if(!compositeDisposable.isDisposed) compositeDisposable.dispose()
    }
}