package com.cochi.kard.app

import com.cochi.kard.core.base.BaseSingleInteractor
import com.cochi.kard.data.User
import com.cochi.kard.data.UserRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetUsersInteractor @Inject constructor(
    private val userRepository: UserRepository
) : BaseSingleInteractor<List<User>>(
    Schedulers.io(), AndroidSchedulers.mainThread()
) {
    private var id: String = ""

    override fun getSingle(): Single<List<User>> {
        return if (id.isEmpty()) userRepository.getUsers() else userRepository.getUser(id)
    }

    fun getUsers(onSuccess: (List<User>) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return subscribe(onSuccess, onError)
    }

    fun getUser(
        userId: String,
        onSuccess: (List<User>) -> Unit,
        onError: (Throwable) -> Unit
    ): Disposable {
        id = userId
        return subscribe(onSuccess, onError)
    }
}