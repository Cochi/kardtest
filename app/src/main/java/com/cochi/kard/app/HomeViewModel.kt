package com.cochi.kard.app

import com.cochi.kard.R
import com.cochi.kard.app.base.BaseViewModel
import com.cochi.kard.core.realm.DatabaseHandler
import com.cochi.kard.data.Transaction
import com.cochi.kard.data.TransactionRepository
import com.cochi.kard.data.User
import io.reactivex.BackpressureStrategy
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.w3c.dom.Entity
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class HomeViewModel @Inject constructor(
    private val getUsersInteractor: GetUsersInteractor,
    private val getTransactionInteractor: GetTransactionInteractor,
    private val transactionRepository: TransactionRepository
) : BaseViewModel<HomeViewModel.State>() {

    override val defaultState: State get() = State()

    companion object {
        const val DECIMAL_FORMAT = "####0.00"
    }

    private var currentUserId: String = ""
    private var transactionDisposable: Disposable? = null

    fun getUsers() {
        reduce { it.copy(step = Step.Loading) }
        toCompositeDisposable(getUsersInteractor.getUsers(::onGetUserSuccess, ::error))
    }

    fun synchronise(userId: String = currentUserId) {
        toCompositeDisposable(
            transactionRepository.synchronise(userId)
                .subscribeOn(AndroidSchedulers.from(DatabaseHandler.looper))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getTransaction(userId)
                }, ::error)
        )
    }

    fun getTransaction(userId: String = currentUserId) {
        currentUserId = userId

        if (transactionDisposable == null || transactionDisposable?.isDisposed == true) {
            transactionDisposable = getTransactionInteractor.getTransaction(
                userId,
                ::onGetTransactionSuccess,
                ::error
            )
            transactionDisposable?.let { toCompositeDisposable(it) }
        }
    }

    private fun onGetUserSuccess(users: List<User>) {
        reduce { it.copy(step = Step.UserFetched(users)) }
    }

    private fun onGetTransactionSuccess(transactions: List<Transaction>) {
        toCompositeDisposable(
            Single.fromCallable { getEntities(transactions) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ entities ->
                    reduce { it.copy(step = Step.DisplayTransaction(entities)) }
                }, ::error)
        )
    }

    private fun getEntities(transactions: List<Transaction>): MutableList<TransactionEntity> {
        val orderedTransaction = transactions
            .sortedByDescending { it.processedDate }
            .groupBy {
                it.processedDate?.let {
                    SimpleDateFormat("w", Locale.getDefault()).format(it)
                }
            }

        val entities = mutableListOf<TransactionEntity>()

        val today = Calendar.getInstance().time
        val todayWeek = SimpleDateFormat("w", Locale.getDefault()).format(today)

        orderedTransaction.keys.forEach { key ->
            key?.let {

                if (it == todayWeek) {
                    val todayTransaction =
                        orderedTransaction[it]?.filter { it.processedDate?.compareTo(today) == 0 }
                    val weekTransaction =
                        orderedTransaction[it]?.filter { it.processedDate?.compareTo(today) != 0 }

                    addEntity(it.toInt(), todayWeek.toInt(), todayTransaction, entities, true)
                    addEntity(it.toInt(), todayWeek.toInt(), weekTransaction, entities)

                } else {
                    addEntity(it.toInt(), todayWeek.toInt(), orderedTransaction[it], entities)
                }
            }
        }
        return entities
    }

    private fun addEntity(
        transactionWeek: Int,
        todayWeek: Int,
        transactions: List<Transaction>?,
        entities: MutableList<TransactionEntity>,
        isToday: Boolean = false
    ) {
        val sectionTitle = when {
            isToday -> R.string.today
            todayWeek - transactionWeek == 0 -> R.string.this_week
            todayWeek - transactionWeek == 1 -> R.string.last_week
            else -> R.string.week_before
        }

        if (transactions?.isEmpty() == true) return

        val sectionId = if (isToday) "Today $todayWeek" else transactionWeek.toString()

        val decimalFormat = DecimalFormat(DECIMAL_FORMAT)

        transactions?.let {
            entities.add(
                TransactionEntity.Section(
                    sectionId,
                    sectionTitle,
                    todayWeek - transactionWeek,
                    decimalFormat.format(it.sumByDouble { it.price })
                )
            )
            it.forEach { transaction -> entities.add(TransactionEntity.Item(transaction)) }
        }
    }

    data class State(val step: Step = Step.EmptyState)

    sealed class Step {
        object EmptyState : Step()
        object Loading : Step()
        class UserFetched(val users: List<User> = listOf()) : Step()
        class DisplayTransaction(val transactions: MutableList<TransactionEntity> = mutableListOf()) :
            Step()
    }

    sealed class TransactionEntity(val id: String, val viewType: Type) {

        class Section(sectionId: String, val title: Int, val weekDiff: Int, val price: String) :
            TransactionEntity(
                sectionId,
                Type.SECTION
            )

        class Item(val transaction: Transaction) : TransactionEntity(transaction.id, Type.ITEM)
    }

    enum class Type {
        SECTION, ITEM
    }
}