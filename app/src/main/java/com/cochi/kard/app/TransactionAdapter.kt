package com.cochi.kard.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.cochi.kard.R
import com.cochi.kard.app.base.BaseAdapter
import kotlinx.android.synthetic.main.item_transaction.view.*
import kotlinx.android.synthetic.main.item_transaction_section.view.*
import kotlinx.android.synthetic.main.item_transaction_section.view.section_title

class TransactionAdapter : BaseAdapter<TransactionAdapter.ViewHolder>() {

    var items: MutableList<HomeViewModel.TransactionEntity> = mutableListOf()
        set(value) {
            if(value == items) return
            val oldItems = mutableListOf<HomeViewModel.TransactionEntity>().apply {
                addAll(field)
            }

            items.clear()
            items.addAll(value)
            applyDiffUtil(oldItems, items) { old, new -> old.id == new.id }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            HomeViewModel.Type.SECTION.ordinal -> ViewHolder.SectionViewHolder(parent)
            else -> ViewHolder.ItemViewHolder(parent)
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.hashCode().toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].viewType.ordinal
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    sealed class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        abstract fun onBind(entity: HomeViewModel.TransactionEntity)

        class SectionViewHolder(itemView: View) : ViewHolder(itemView) {

            private val title: TextView get() = itemView.section_title
            private val price: TextView get() = itemView.section_price

            constructor(parent: ViewGroup) : this(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_transaction_section, parent, false)
            )

            override fun onBind(entity: HomeViewModel.TransactionEntity) {
                entity as HomeViewModel.TransactionEntity.Section
                val sectionTitle = itemView.context.getString(entity.title)
                title.text = String.format(sectionTitle, entity.weekDiff)
                price.text = entity.price
            }
        }

        class ItemViewHolder(itemView: View) : ViewHolder(itemView) {

            private val icon: ImageView get() = itemView.photo
            private val title: TextView get() = itemView.title
            private val description: TextView get() = itemView.description
            private val price: TextView get() = itemView.price

            constructor(parent: ViewGroup) : this(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_transaction, parent, false)
            )

            override fun onBind(entity: HomeViewModel.TransactionEntity) {
                entity as HomeViewModel.TransactionEntity.Item
                title.text = entity.transaction.title
                description.text = entity.transaction.category
                price.isSelected = entity.transaction.price > 0
                price.text = entity.transaction.price.toString()

                Glide.with(itemView)
                    .load(entity.transaction.photoUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(icon);
            }
        }
    }
}