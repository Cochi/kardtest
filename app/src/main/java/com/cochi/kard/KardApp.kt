package com.cochi.kard

import android.app.Application
import com.cochi.kard.core.realm.RealmModule
import io.realm.Realm
import io.realm.RealmConfiguration

class KardApp : Application() {

    lateinit var appComponent: AppComponent

    companion object {
        private const val DATABASE_NAME = "kard.data"
    }


    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.create()

        Realm.init(this)

        val config = RealmConfiguration.Builder()
            .name(DATABASE_NAME)
            .addModule(RealmModule())
            .schemaVersion(1)
            .build()
        Realm.setDefaultConfiguration(config)
    }
}