package com.cochi.kard

import com.cochi.kard.app.HomeComponent
import com.cochi.kard.core.dagger.NetworkModule
import com.cochi.kard.core.dagger.SubComponentsModule
import dagger.Component
import dagger.Subcomponent
import javax.inject.Singleton

@Singleton
@Component(modules = [SubComponentsModule::class, NetworkModule::class])
interface AppComponent {
    fun provideHomeComponent() : HomeComponent.Factory
}