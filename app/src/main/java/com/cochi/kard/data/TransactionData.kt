package com.cochi.kard.data

import com.cochi.kard.app.HomeViewModel
import com.cochi.kard.data.realm.RTransaction
import com.google.gson.annotations.SerializedName
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.realm.Realm
import retrofit2.http.GET
import retrofit2.http.Path
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TransactionRepository @Inject constructor(
    private val userRemoteDataSource: TransactionRemoteDataSource,
    private val userLocalDataSource: TransactionLocalDataSource,
) {
    fun synchronise(userId: String): Single<List<Transaction>> {
        return userRemoteDataSource.getTransactions(userId)
            .doOnSuccess { userLocalDataSource.save(it) }
    }

    fun getTransactions(userId: String): Flowable<List<Transaction>> {
        return Flowable.fromCallable {  userLocalDataSource.get(userId)}
    }

}

@Singleton
class TransactionRemoteDataSource @Inject constructor(
    private val userService: TransactionService
) {

    fun getTransactions(userId: String): Single<List<Transaction>> {
        return userService.getTransactions(userId).map { users -> users.map { it.toEntity() } }
    }
}

@Singleton
class TransactionLocalDataSource @Inject constructor() {

    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX"
    }

    fun save(transactions: List<Transaction>) {
        Realm.getDefaultInstance().executeTransactionAsync {
            transactions.forEach { transaction ->
                it.insertOrUpdate(toRealm(transaction))
            }
        }
    }

    fun get(userId: String):List<Transaction> {
        val transactions = mutableListOf<Transaction>()

        Realm.getDefaultInstance().executeTransaction {
            val result = it.where(RTransaction::class.java)
                .equalTo("userId", userId)
                .findAllAsync()

            result.forEach{
                transactions.add(toObject(it))
            }
        }

        return transactions
    }

    private fun toObject(transaction: RTransaction): Transaction {
        val dateFormat: DateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

        return Transaction(
            transaction.id,
            transaction.userId,
            transaction.createdAt,
            transaction.processedAt,
            transaction.title,
            transaction.category,
            transaction.price,
            transaction.photoUrl,
            dateFormat.parse(transaction.processedAt)
        )
    }

    private fun toRealm(transaction: Transaction): RTransaction {

        return RTransaction(
            transaction.id,
            transaction.userId,
            transaction.createdAt,
            transaction.processedAt,
            transaction.title,
            transaction.category,
            transaction.price,
            transaction.photoUrl
        )
    }


}

interface TransactionService {
    @GET("users/{userId}/transactions?_sort=createdAt&_order=desc")
    fun getTransactions(@Path("userId") userId: String): Single<List<TransactionJson>>

}

data class TransactionJson(
    @SerializedName("id") val id: String,
    @SerializedName("userId") val userId: String,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("processedAt") val processedAt: String,
    @SerializedName("title") val title: String,
    @SerializedName("category") val category: String,
    @SerializedName("price") val price: Double,
    @SerializedName("photo") val photoUrl: String
) {
    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX"
    }

    fun toEntity(): Transaction {
        val dateFormat: DateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

        return Transaction(
            id,
            userId,
            createdAt,
            processedAt,
            title,
            category,
            price,
            photoUrl,
            dateFormat.parse(processedAt)
        )
    }
}

data class Transaction(
    val id: String,
    val userId: String,
    val createdAt: String,
    val processedAt: String,
    val title: String,
    val category: String,
    val price: Double,
    val photoUrl: String,
    val processedDate: Date?

) {

    fun toJson(): TransactionJson {
        return TransactionJson(id, userId, createdAt, processedAt, title, category, price, photoUrl)
    }
}