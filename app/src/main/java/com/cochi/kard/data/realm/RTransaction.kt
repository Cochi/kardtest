package com.cochi.kard.data.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RTransaction (
    @PrimaryKey open var id: String = "",
    open var userId: String ="",
    open var createdAt: String ="",
    open var processedAt: String ="",
    open var title: String ="",
    open var category: String ="",
    open var price: Double = 0.0,
    open var photoUrl: String ="",
) : RealmObject() {
}