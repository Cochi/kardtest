package com.cochi.kard.data

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UserRepository @Inject constructor(
    private val userRemoteDataSource: UserRemoteDataSource
) {
    fun getUsers(): Single<List<User>> {
        return userRemoteDataSource.getUsers()
    }

    fun getUser(id: String) : Single<List<User>> {
        return userRemoteDataSource.getUser(id)
    }
}

@Singleton
class UserRemoteDataSource @Inject constructor(
    private val userService: UserService
) {

    fun getUsers(): Single<List<User>> {
        return userService.getUsers().map { users -> users.map { it.toEntity() } }
    }

    fun getUser(id:String): Single<List<User>> {
        return userService.getUser(id).map { listOf(it.toEntity()) }
    }
}

interface UserService {

    @GET("users")
    fun getUsers(): Single<List<UserJson>>

    @GET("users/{id}")
    fun getUser(@Path("id") id: String): Single<UserJson>
}

data class UserJson(
    @SerializedName("id") val id: String,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("email") val email: String,
    @SerializedName("avatar") val avatarPath: String,
    @SerializedName("balance") val transactionBalance: Double
) {
    fun toEntity(): User {
        return User(id, firstName, lastName, email, avatarPath, transactionBalance)
    }
}

data class User(
    val id: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val avatarPath: String,
    val transactionBalance: Double
) {

    fun toJson(): UserJson {
        return UserJson(id, firstName, lastName, email, avatarPath, transactionBalance)
    }
}