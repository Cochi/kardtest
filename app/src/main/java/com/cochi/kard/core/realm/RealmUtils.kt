package com.cochi.kard.core.realm

import android.os.HandlerThread
import android.os.Process.THREAD_PRIORITY_BACKGROUND


object DatabaseHandler : HandlerThread("DatabaseHandler", THREAD_PRIORITY_BACKGROUND) {
    init {
        start()
    }
}
