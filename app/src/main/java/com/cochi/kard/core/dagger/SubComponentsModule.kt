package com.cochi.kard.core.dagger

import com.cochi.kard.app.HomeComponent
import dagger.Module
import dagger.Subcomponent

@Module(subcomponents = [HomeComponent::class])
class SubComponentsModule {
}