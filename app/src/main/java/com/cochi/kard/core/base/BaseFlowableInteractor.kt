package com.cochi.kard.core.base

import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable

abstract class BaseFlowableInteractor<T>(
    private val subscribeScheduler: Scheduler,
    private val observeScheduler: Scheduler
) {

    abstract fun getFlowable(): Flowable<T>

    fun subscribe(onSuccess:(T) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return getFlowable().subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
            .subscribe(onSuccess, onError)
    }
}