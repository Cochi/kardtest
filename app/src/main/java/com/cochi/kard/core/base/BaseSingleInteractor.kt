package com.cochi.kard.core.base

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable

abstract class BaseSingleInteractor<T>(
    private val subscribeScheduler: Scheduler,
    private val observeScheduler: Scheduler
) {

    abstract fun getSingle(): Single<T>

    fun subscribe(onSuccess:(T) -> Unit, onError: (Throwable) -> Unit): Disposable {
        return getSingle().subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
            .subscribe(onSuccess, onError)
    }
}