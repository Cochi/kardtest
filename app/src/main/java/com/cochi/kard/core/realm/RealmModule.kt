package com.cochi.kard.core.realm

import io.realm.annotations.RealmModule

@RealmModule(allClasses = true, library = true)
class RealmModule {
}